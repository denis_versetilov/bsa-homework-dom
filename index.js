
function getCharacters() {
  const url = 'https://rickandmortyapi.com/api/character';

  return fetch(url)
    .then(res => res.json())
    .then(data => {
      const rawData = data.results;
      console.log(rawData);
      return rawData;
      /* return rawData.map(character => {
         //all needed data is listed below as an entity 
         let created = character.created;
               species = character.species ;
               img = character.img;
               episodes = character.episode;
               name = character.name;
               location = character.location;
           //create element
           //append element
       }); */
    })

    .catch((error) => {
      console.log(JSON.stringify(error));
    });
}

class CharacterService {
  async getCharacters() {
    try {
      const apiResult = await getCharacters();
      console.log(apiResult);
      return apiResult;
      //return JSON.parse(atob(apiResult.content));
    } catch (error) {
      throw error;
    }
  }
}

const characterService = new CharacterService();


class App {
  constructor() {
    this.startApp();
  }

  static rootElement = document.getElementsByClassName('w3-main')[0];

  async startApp() {
    try {
      const characters = await characterService.getCharacters();
      console.log(characters);
      const charactersView = new CharactersView(characters);
      const charactersElement = charactersView.element;

      App.rootElement.appendChild(charactersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    }
  }
}

new App();

class View {
  element;

  createElement({ tagName, className = '', attributes = {} }) {
    const element = document.createElement(tagName);
    element.classList.add(className);

    Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

    return element;

  }
}
class CharacterView extends View {
  constructor(character) {
    super();

    this.createCharacter(character);
  }

  createCharacter(character) {
    const { species, image, episode, name, location, created } = character;
    const imgElement = this.createImg(image);
    const speciesElement = this.createSpecies(species);
    const episodeElement = this.createEpisode(episode.length);
    const nameElement = this.createName(name);
    const locationElement = this.createLocation(location.name);
    const createdElement = this.createCreated(created)

    this.element = this.createElement({ tagName: 'div', className: 'w3-quarter' });
    this.element.append(imgElement, nameElement, locationElement, speciesElement, episodeElement, createdElement);
  }

  createImg(image) {
    const attributes = { src: image };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'character-img',
      attributes
    });

    return imgElement;
  }

  createName(name) {
    const nameElement = this.createElement({
      tagName: 'div',
      className: 'name',
    });
    nameElement.innerText = name;
    return nameElement;
  }

  createSpecies(species) {
    const speciesElement = this.createElement({
      tagName: 'div',
      className: 'species'
    });
    speciesElement.innerText = species;

    return speciesElement;
  }


  createLocation(location) {
    const locationElement = this.createElement({
      tagName: 'div',
      className: 'location'
    });
    locationElement.innerText = location;

    return locationElement;
  }


  createEpisode(episode) {
    const episodeElement = this.createElement({
      tagName: 'div',
      className: 'episode',
    });
    episodeElement.innerText = episode;
    return episodeElement;
  }

  createCreated(created) {
    const createdElement = this.createElement({
      tagName: 'div',
      className: 'created',
    });
    createdElement.innerText = created.substring(0, 16);
    return createdElement;
  }


}

class CharactersView extends View {
  constructor(characters) {
    super();
    this.createCharacters(characters);
  }

  charactersDetailsMap = new Map();

  createCharacters(characters) {
    console.log("here");
    console.log(
      characters
    );
    const characterElements = characters.map(character => {
      const characterView = new CharacterView(character);
      return characterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'characters' });
    this.element.append(...characterElements);
  }
}



function sortDate() {
  var nodeList = document.querySelectorAll('div.created');
  var itemsArray = [];
  var parent = nodeList[0].parentNode;
  for (var i = 0; i < nodeList.length; i++) {
    itemsArray.push(parent.removeChild(nodeList[i]));
  }
  itemsArray.sort(function (nodeA, nodeB) {
    var textA = nodeA.querySelector('div:nth-child(2)').textContent;
    var textB = nodeB.querySelector('div:nth-child(2)').textContent;
    var numberA = parseInt(textA);
    var numberB = parseInt(textB);
    if (numberA < numberB) return -1;
    if (numberA > numberB) return 1;
    return 0;
  })
    .forEach(function (node) {
      parent.appendChild(node)
    });
}

console.clear();

function sort(reverse) {
  var nodeList = document.querySelectorAll('div.created');
  var itemsArray = [];
  var parent = nodeList[0].parentNode;
  for (var i = 0; i < nodeList.length; i++) {
    itemsArray.push(parent.removeChild(nodeList[i]));
  }
  itemsArray.sort(function (nodeA, nodeB) {
    var textA = nodeA.querySelector('div:nth-child(2)').textContent;
    var textB = nodeB.querySelector('div:nth-child(2)').textContent;
    var numberA = parseInt(textA);
    var numberB = parseInt(textB);
    if (numberA < numberB) {
      return reverse ? 1 : -1;
    }
    if (numberA > numberB) {
      return reverse ? -1 : 1;
    }
    return 0;
  })
    .forEach(function (node) {
      parent.appendChild(node)
    });
    }
